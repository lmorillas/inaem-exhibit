{
    "items": [{
        "cursos": 3,
        "horas": 780,
        "type": "ent",
        "id": "658 H",
        "label": "INSTITUTO DE EDUCACION SECUNDARIA RIO GALLEGO"
    }, {
        "cursos": 1,
        "horas": 210,
        "type": "ent",
        "id": "209 H",
        "label": "INSTITUTO DE PSICOLOGIA ASESORES SA"
    }, {
        "cursos": 3,
        "horas": 770,
        "type": "ent",
        "id": "170 H",
        "label": "FORMACION Y ASESORES EN SELECCION Y EMPLEO SL"
    }, {
        "cursos": 5,
        "horas": 1430,
        "type": "ent",
        "id": "285 H",
        "label": "SOCIEDAD ARAGONESA DE ASESORIA TECNICA SL"
    }, {
        "cursos": 1,
        "horas": 430,
        "type": "ent",
        "id": "417 H",
        "label": "FUNDACION UNIVERSIDAD SAN JORGE"
    }, {
        "cursos": 4,
        "horas": 1230,
        "type": "ent",
        "id": "709 H",
        "label": "CENTRO DE ESTUDIOS SAN MIGUEL DE ZARAGOZA SL"
    }, {
        "cursos": 1,
        "horas": 260,
        "type": "ent",
        "id": "080 H",
        "label": "CENTRO DE ESTUDIOS REINA VICTORIA DE ZARAGOZA SC"
    }, {
        "cursos": 1,
        "horas": 160,
        "type": "ent",
        "id": "232 H",
        "label": "PATRONATO CATOLICO BENEFICO SOCIAL NUESTRA SE\u00d1ORA DE LOS DOLORES"
    }, {
        "cursos": 2,
        "horas": 770,
        "type": "ent",
        "id": "087 H",
        "label": "FUNDACI\u00d3N CIRCE - CENTRO DE INVESTIGACI\u00d3N DE RECURSOS Y CONSUMOS ENERG\u00c9TICOS"
    }, {
        "cursos": 2,
        "horas": 530,
        "type": "ent",
        "id": "215 H",
        "label": "LABORATORIO, ORGANIZACION, GESTION, OPOS. Y SERVICIOS SL"
    }, {
        "cursos": 1,
        "horas": 490,
        "type": "ent",
        "id": "227 H",
        "label": "MUELA ALGARATE, ANGEL"
    }, {
        "cursos": 1,
        "horas": 420,
        "type": "ent",
        "id": "837 H",
        "label": "INSTITUTO HERMANOS SAGRADO CORAZ\u00d3N(CORAZONISTAS)"
    }, {
        "cursos": 1,
        "horas": 210,
        "type": "ent",
        "id": "123 H",
        "label": "COMPUTER TARAZONA SL"
    }, {
        "cursos": 1,
        "horas": 320,
        "type": "ent",
        "id": "340 H",
        "label": "TARAZONA FORMACION, SL"
    }, {
        "cursos": 3,
        "horas": 740,
        "type": "ent",
        "id": "315 H",
        "label": "CENTRO DE ENSE\u00d1ANZAS TECNICAS Y ADMINISTRATIVAS SL"
    }, {
        "cursos": 5,
        "horas": 1050,
        "type": "ent",
        "id": "198 H",
        "label": "IDIOMAS TERUEL SL"
    }, {
        "cursos": 7,
        "horas": 2470,
        "type": "ent",
        "id": "274 H",
        "label": "T & Z FORMACION SL"
    }, {
        "cursos": 1,
        "horas": 320,
        "type": "ent",
        "id": "145 H",
        "label": "CERTI FORMA, S.L."
    }, {
        "cursos": 5,
        "horas": 1690,
        "type": "ent",
        "id": "183 H",
        "label": "FUNDACION SAN VALERO"
    }, {
        "cursos": 1,
        "horas": 280,
        "type": "ent",
        "id": "522 H",
        "label": "INSTITUTO DE INVESTIGACION SOBRE REPARACION DE VEHICULOS SA"
    }, {
        "cursos": 1,
        "horas": 220,
        "type": "ent",
        "id": "391 H",
        "label": "S.C.L. AGROPIENSO"
    }, {
        "cursos": 3,
        "horas": 1450,
        "type": "ent",
        "id": "630 H",
        "label": "GESTORA DE COLOCACION FORMACION EMPRESARIAL SL"
    }, {
        "cursos": 1,
        "horas": 540,
        "type": "ent",
        "id": "400 H",
        "label": "LOGISIT S.L.U."
    }, {
        "cursos": 2,
        "horas": 920,
        "type": "ent",
        "id": "150 H",
        "label": "ESTUDIOS ZARAGOZA SL"
    }, {
        "cursos": 1,
        "horas": 80,
        "type": "ent",
        "id": "933 H",
        "label": "C EDUCACION ADULTOS MIGUEL HERN\u00c1NDEZ DE CASETAS"
    }, {
        "cursos": 2,
        "horas": 820,
        "type": "ent",
        "id": "645 H",
        "label": "CENTRO PUBLICO INTEGRADO DE FOMACION PROFESIONAL LOS ENLACES"
    }, {
        "cursos": 1,
        "horas": 310,
        "type": "ent",
        "id": "411 H",
        "label": "OBRA DIOCESANA SANTO DOMINGO DE SILOS"
    }, {
        "cursos": 3,
        "horas": 990,
        "type": "ent",
        "id": "202 H",
        "label": "METODOS DE CONTROL HIGIENICO SANITARIO, SL"
    }, {
        "cursos": 3,
        "horas": 1070,
        "type": "ent",
        "id": "179 H",
        "label": "FUNDACION LABORAL DE LA CONSTRUCCION"
    }, {
        "cursos": 2,
        "horas": 710,
        "type": "ent",
        "id": "086 H",
        "label": "FEDERACION DE CENTROS DE PROMOCION RURAL (EFAS DE ARAGON)"
    }, {
        "cursos": 4,
        "horas": 1650,
        "type": "ent",
        "id": "959 H",
        "label": "FUNDACI\u00d3N ARAGONESA PARA LA FORMACI\u00d3N Y EL EMPLEO (ARAFOREM)"
    }, {
        "cursos": 2,
        "horas": 490,
        "type": "ent",
        "id": "064 H",
        "label": "AYUNTAMIENTO DE UTEBO"
    }, {
        "cursos": 2,
        "horas": 820,
        "type": "ent",
        "id": "642 H",
        "label": "MASTER DISTANCIA SA"
    }, {
        "cursos": 1,
        "horas": 340,
        "type": "ent",
        "id": "537 H",
        "label": "ESTECFORM SL"
    }, {
        "cursos": 1,
        "horas": 520,
        "type": "ent",
        "id": "370 H",
        "label": "BLASCO ROYO, M\u00aa DOLORES TERESA"
    }, {
        "cursos": 63,
        "horas": 2762,
        "type": "ent",
        "id": "999 H",
        "label": "CENTRO DE TECNOLOG\u00cdAS AVANZADAS"
    }, {
        "cursos": 2,
        "horas": 830,
        "type": "ent",
        "id": "801 H",
        "label": "ATADES HUESCA"
    }, {
        "cursos": 2,
        "horas": 670,
        "type": "ent",
        "id": "646 H",
        "label": "AYUNTAMIENTO DE EJEA DE LOS CABALLEROS"
    }, {
        "cursos": 3,
        "horas": 470,
        "type": "ent",
        "id": "173 H",
        "label": "FORUM ARAGON, SA"
    }, {
        "cursos": 25,
        "horas": 6090,
        "type": "ent",
        "id": "741 H",
        "label": "INSTITUTO MUNICIPAL DE EMPLEO Y FOMENTO EMPRESARIAL"
    }, {
        "cursos": 1,
        "horas": 490,
        "type": "ent",
        "id": "806 H",
        "label": "ACADEMIA TECNICA OSCENSE SL"
    }, {
        "cursos": 10,
        "horas": 2910,
        "type": "ent",
        "id": "211 H",
        "label": "METAFOR 2000 SL"
    }, {
        "cursos": 1,
        "horas": 160,
        "type": "ent",
        "id": "386 H",
        "label": "INSTITUTO TUROLENSE DE FORMACION EMPRESARIAL SL"
    }, {
        "cursos": 7,
        "horas": 2630,
        "type": "ent",
        "id": "155 H",
        "label": "ADES CENTRO TECNOL\u00d3GICO, S L"
    }, {
        "cursos": 3,
        "horas": 1270,
        "type": "ent",
        "id": "550 H",
        "label": "ARQUISOCIAL, S.L."
    }, {
        "cursos": 1,
        "horas": 220,
        "type": "ent",
        "id": "189 H",
        "label": "GLORIMAR SL"
    }, {
        "cursos": 1,
        "horas": 360,
        "type": "ent",
        "id": "821 H",
        "label": "AGRUPACION DE ESTUDIOS ZARAGOZA SL"
    }, {
        "cursos": 2,
        "horas": 740,
        "type": "ent",
        "id": "338 H",
        "label": "ORDESACTIVA, SL"
    }, {
        "cursos": 1,
        "horas": 260,
        "type": "ent",
        "id": "527 H",
        "label": "KERNEL ENGLISH CENTRE SCL"
    }, {
        "cursos": 2,
        "horas": 520,
        "type": "ent",
        "id": "058 H",
        "label": "ESCUELA DE ESTUDIOS SUPERIORES ESIC"
    }, {
        "cursos": 1,
        "horas": 430,
        "type": "ent",
        "id": "449 H",
        "label": "BONFIL FACI, MARIA TERESA"
    }, {
        "cursos": 6,
        "horas": 1610,
        "type": "ent",
        "id": "737 H",
        "label": "CENTRO DE FORMACION DE CONDUCTORES OLIVAN SL"
    }, {
        "cursos": 3,
        "horas": 1200,
        "type": "ent",
        "id": "890 H",
        "label": "FEDERACION EMPRESARIOS DEL METAL DE HUESCA"
    }, {
        "cursos": 1,
        "horas": 270,
        "type": "ent",
        "id": "334 H",
        "label": "HACER CREATIVO, SL"
    }, {
        "cursos": 1,
        "horas": 520,
        "type": "ent",
        "id": "867 H",
        "label": "CONFEDERACI\u00d3N EMPRESARIAL TUROLENSE"
    }, {
        "cursos": 1,
        "horas": 510,
        "type": "ent",
        "id": "706 H",
        "label": "IBERCAJA OBRA SOCIAL"
    }, {
        "cursos": 1,
        "horas": 480,
        "type": "ent",
        "id": "616 H",
        "label": "V.I.D.A. (VERSATILIDAD E INNOVACI\u00d3N PARA DESARROLLO AGROAMBIENTAL)"
    }, {
        "cursos": 1,
        "horas": 520,
        "type": "ent",
        "id": "722 H",
        "label": "MUELA BAUTO SL"
    }, {
        "cursos": 1,
        "horas": 490,
        "type": "ent",
        "id": "529 H",
        "label": "CENTRO DE FORMACION VIRGEN DEL CASTILLO SL"
    }, {
        "cursos": 1,
        "horas": 370,
        "type": "ent",
        "id": "393 H",
        "label": "WORKING FORMACI\u00d3N INTEGRAL S.L."
    }, {
        "cursos": 3,
        "horas": 970,
        "type": "ent",
        "id": "816 H",
        "label": "ARAG\u00d3N FORMACI\u00d3N ACF, SL"
    }, {
        "cursos": 1,
        "horas": 480,
        "type": "ent",
        "id": "998 H",
        "label": "ASOCIACI\u00d3N CULTURAL Y DEPORTIVA OC\u00c9ANO ATL\u00c1NTICO"
    }, {
        "cursos": 2,
        "horas": 820,
        "type": "ent",
        "id": "895 H",
        "label": "CENTRO DE FORMACION POLO, SL"
    }, {
        "cursos": 6,
        "horas": 2730,
        "type": "ent",
        "id": "234 H",
        "label": "QUALITAS MANAGEMENT SL"
    }, {
        "cursos": 1,
        "horas": 525,
        "type": "ent",
        "id": "051 H",
        "label": "ASOCIACION PROVINCIAL EMPRESARIAL DE FONTANERIA"
    }, {
        "cursos": 1,
        "horas": 435,
        "type": "ent",
        "id": "153 H",
        "label": "EUROFOR CENTRO DE FORMACION SL"
    }, {
        "cursos": 2,
        "horas": 800,
        "type": "ent",
        "id": "001 H",
        "label": "CONFEDERACION DE EMPRESARIOS DE ARAGON (CREA)"
    }, {
        "cursos": 1,
        "horas": 100,
        "type": "ent",
        "id": "208 H",
        "label": "INSTITUTO DE EDUCACION SECUNDARIA MIRALBUENO DE ZARAGOZA"
    }, {
        "cursos": 4,
        "horas": 2030,
        "type": "ent",
        "id": "332 H",
        "label": "INSTITUTO DE FORMACION Y ESTUDIOS SOCIALES"
    }, {
        "cursos": 1,
        "horas": 520,
        "type": "ent",
        "id": "820 H",
        "label": "CENTRO DE ESTUDIOS PROFESIONALES DE ARAGON SA"
    }, {
        "cursos": 2,
        "horas": 1010,
        "type": "ent",
        "id": "699 H",
        "label": "CENTRO DE FORMACION HUESCA SC"
    }, {
        "cursos": 2,
        "horas": 590,
        "type": "ent",
        "id": "046 H",
        "label": "INSTITUO DE FORMACI\u00d3N PROFESIONAL ESPEC\u00cdFICA DE MOVERA"
    }, {
        "cursos": 1,
        "horas": 330,
        "type": "ent",
        "id": "313 H",
        "label": "AUTO ESCUELA CATALU\u00d1A SL"
    }, {
        "cursos": 2,
        "horas": 710,
        "type": "ent",
        "id": "002 H",
        "label": "CONFEDERACION DE LA PEQUE\u00d1A Y MEDIANA EMPRESA ARAGONESA (CEPYME)"
    }, {
        "cursos": 1,
        "horas": 435,
        "type": "ent",
        "id": "212 H",
        "label": "IZQUIERDO CENTRO DE INFORMATICA SL"
    }, {
        "cursos": 1,
        "horas": 230,
        "type": "ent",
        "id": "320 H",
        "label": "TURSPORT 2000 SLL"
    }, {
        "cursos": 1,
        "horas": 520,
        "type": "ent",
        "id": "329 H",
        "label": "PLUS ULTRA FORMACI\u00d3N, S.L."
    }, {
        "cursos": 1,
        "horas": 210,
        "type": "ent",
        "id": "048 H",
        "label": "FUNDACION APIP-ACAM"
    }, {
        "cursos": 1,
        "horas": 570,
        "type": "ent",
        "id": "558 H",
        "label": "CENTRO DE FORMACION RIBAGORZA SL"
    }, {
        "cursos": 2,
        "horas": 600,
        "type": "ent",
        "id": "164 H",
        "label": "FEDERACION DE INDUSTRIAS TEXTILES Y DE LA CONFECCION DE ARAGON"
    }, {
        "cursos": 13,
        "horas": 1600,
        "type": "ent",
        "id": "904 H",
        "label": "CENTRO DE FORMACI\u00d3N PARA EL EMPLEO DE HUESCA"
    }, {
        "cursos": 1,
        "horas": 260,
        "type": "ent",
        "id": "238 H",
        "label": "TECHNICAL COLLEGE SL"
    }, {
        "cursos": 1,
        "horas": 430,
        "type": "ent",
        "id": "310 H",
        "label": "CENTRO ESTUDIOS MARCIAL SL"
    }, {
        "cursos": 2,
        "horas": 610,
        "type": "ent",
        "id": "812 H",
        "label": "AUTOESCUELA PEGASUS SA"
    }, {
        "cursos": 1,
        "horas": 380,
        "type": "ent",
        "id": "345 H",
        "label": "CONFEDERACION EMPRESARIAL DE LA PROVINCIA DE HUESCA"
    }, {
        "cursos": 2,
        "horas": 650,
        "type": "ent",
        "id": "223 H",
        "label": "MERCADOS CENTRALES DE ABASTECIMIENTO DE ZARAGOZA SA"
    }, {
        "cursos": 1,
        "horas": 290,
        "type": "ent",
        "id": "754 H",
        "label": "MARIANO TORRECILLA INSA"
    }, {
        "cursos": 2,
        "horas": 680,
        "type": "ent",
        "id": "271 H",
        "label": "FUNDACI\u00d3N CPA SALDUIE"
    }, {
        "cursos": 1,
        "horas": 260,
        "type": "ent",
        "id": "923 H",
        "label": "ACCIONES T\u00c9CNICAS BAJO ARAGONESA 2005 SL"
    }, {
        "cursos": 1,
        "horas": 240,
        "type": "ent",
        "id": "124 H",
        "label": "FUNDACI\u00d3N CRUZ BLANCA-DELEGACI\u00d3N ARAG\u00d3N"
    }, {
        "cursos": 4,
        "horas": 700,
        "type": "ent",
        "id": "563 H",
        "label": "CENTRO PUBLICO INTEGRADO DE FORMACION PROFESIONAL CORONA DE ARAGON"
    }, {
        "cursos": 3,
        "horas": 820,
        "type": "ent",
        "id": "970 H",
        "label": "FUNDACI\u00d3N DFA"
    }, {
        "cursos": 1,
        "horas": 480,
        "type": "ent",
        "id": "006 H",
        "label": "ACADEMIA MARCO SA"
    }, {
        "cursos": 4,
        "horas": 1315,
        "type": "ent",
        "id": "075 H",
        "label": "CASA SALESIANA DE NUESTRA SE\u00d1ORA DEL PILAR COLEGIO SALESIANO NTRA SRA DEL PILAR DE ZARAGOZA"
    }, {
        "cursos": 1,
        "horas": 220,
        "type": "ent",
        "id": "697 H",
        "label": "SISTEMAS EDUCATIVOS PIVOT POINT SL"
    }, {
        "cursos": 1,
        "horas": 100,
        "type": "ent",
        "id": "081 H",
        "label": "CENTRO DE ESTUDIOS SUPERIORES Y TECNICOS DE LA EMPRESA SL"
    }, {
        "cursos": 1,
        "horas": 100,
        "type": "ent",
        "id": "637 H",
        "label": "CPEPA JOAQU\u00cdN COSTA"
    }, {
        "cursos": 1,
        "horas": 440,
        "type": "ent",
        "id": "966 H",
        "label": "KAIROS SOCIEDAD COOPERATIVA DE INICIATIVA SOCIAL"
    }, {
        "cursos": 4,
        "horas": 1515,
        "type": "ent",
        "id": "146 H",
        "label": "ESCUELA PROFESIONAL LA SALLE SANTO ANGEL EN VALDEFIERRO ESCUELAS CRISTIANAS HERMANOS PR (FSC)"
    }, {
        "cursos": 1,
        "horas": 380,
        "type": "ent",
        "id": "380 H",
        "label": "AUTOESCUELA TUROLENSE, SL"
    }, {
        "cursos": 1,
        "horas": 300,
        "type": "ent",
        "id": "844 H",
        "label": "CENTRO PUBLICO INTEGRADO DE FORMACION PROFESIONAL PIRAMIDE"
    }, {
        "cursos": 5,
        "horas": 1400,
        "type": "ent",
        "id": "005 H",
        "label": "K\u00dcHNEL FORMACION SA"
    }, {
        "cursos": 1,
        "horas": 60,
        "type": "ent",
        "id": "096 H",
        "label": "JOVENES AGRICULTORES  DEL \"ALTO ARAGON\""
    }, {
        "cursos": 5,
        "horas": 2500,
        "type": "ent",
        "id": "014 H",
        "label": "ASOCIACION ARAGONESA DE COOP Y SOCIEDADES LABORALES"
    }, {
        "cursos": 1,
        "horas": 200,
        "type": "ent",
        "id": "161 H",
        "label": "MCSYSTEM S.C."
    }, {
        "cursos": 2,
        "horas": 790,
        "type": "ent",
        "id": "182 H",
        "label": "FUNDACION RAMON REY ARDID"
    }, {
        "cursos": 3,
        "horas": 1050,
        "type": "ent",
        "id": "423 H",
        "label": "SERRANO FARADUES CONSULTORES INGENIEROS, SL"
    }, {
        "cursos": 2,
        "horas": 270,
        "type": "ent",
        "id": "342 H",
        "label": "INAGROPEC SL"
    }, {
        "cursos": 2,
        "horas": 460,
        "type": "ent",
        "id": "817 H",
        "label": "YMCA"
    }, {
        "cursos": 1,
        "horas": 410,
        "type": "ent",
        "id": "131 H",
        "label": "CRUZ ROJA ESPA\u00d1OLA"
    }, {
        "cursos": 2,
        "horas": 270,
        "type": "ent",
        "id": "325 H",
        "label": "CENTRO ALAUN SL"
    }],
    "properties": {
        "cursos": {
            "valueType": "number"
        },
        "horas": {
            "valueType": "number"
        }
    },
    "types": {
        "ent": {
            "pluralLabel": "Entidades",
            "label": "Entidad"
        }
    }
}
