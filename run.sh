#!/bin/sh

RUTA=/home/lm/src/inaem-exhibit
DESTINO=/usr/share/nginx/html/inaem/
cd $RUTA

. $RUTA/env/bin/activate


python crea_exhibit.py

cp inaem.js $DESTINO
cp entidades.js  $DESTINO
cp index.html  $DESTINO
cp entidades.html $DESTINO
