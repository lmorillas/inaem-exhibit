# coding: utf-8

import json
from geopy import geocoders
from datetime import datetime
import locale

lcur = json.load(open('inaem.json'), encoding="utf-8")
lcentros = json.load(open('inaem_centros.js'))

metadatos = { "types": {
                        "curso": {
                            "pluralLabel": "Cursos",
                            "Label": "Curso"
                        },
                        "centro": {
                            "pluralLabel": "Centros",
                            "Label": "Centro"
                        },
                        "ent" : {
                        "Label": "Entidad",
                        "pluralLabel": "Entidades"
                        }
             },
             "properties": {
                        "id_centro": {
                            "valueType": "centro",
                            "label": "en centro",
                            "reverseLabel": "imparte"
                            }
                        }
            }

metadatos_ent = { "types": {
                        "ent" : {
                        "label": "Entidad",
                        "pluralLabel": "Entidades"
                        }
             },

    "properties": {
        "horas": {
            "valueType": "number"
        },
        "cursos": {
            "valueType": "number"
        }
    }

}

'''
for n,c in enumerate(lcur):
    c[u'label']=n+1
'''

_centros = []
_entidades = {}

cursos = {'items':lcur}

gc = geocoders.GoogleV3()

def id_curso(url):
    '''devuelve id curso a partir de url'''
    return url.replace ("https://plan.aragon.es/MapaRec.nsf/(ID)/", '').replace('?OpenDocument', '')

def id_entidad(entidad):
    pos = entidad.find(' H ')
    return entidad[:pos+2]

def nombre_centro(nombre):
    pos = nombre.find(' H ')
    return nombre[pos+2:].strip()

def busca_horario(curso):
    if u'mañana y tarde' in curso.get('fecha').lower():
        return 'mañana y tarde'
    if u'mañana' in curso.get('fecha').lower():
        return u'mañana'
    if u'tarde' in curso.get('fecha').lower():
        return u'tarde'

def mes_inicio(curso):
    meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
    mes = curso.get('fecha_inicio')

    if mes:
        mes = mes.split('/')[1]
        return meses[int(mes) - 1]
    else:
        return ''

for n, c in enumerate(lcur):
    centro = c.get('centro')


    if centro not in lcentros:
        loc = c.get('direccion')
        print 'Buscando', loc
        l = gc.geocode(loc)
        if l:
            lcentros[centro] = {'localizacion':l.address,
                                'latlon': "{},{}".format(l.latitude, l.longitude)}

    c['url'] = id_curso(c.get('url'))
    c['label'] = c['nombre']
    c['id'] = n
    #try:
    #c['localizacion'] = lcentros[centro]['localizacion']
    #c['latlon'] = lcentros[centro]['latlon']


    if not c.get('horario'):
        #print "sin horario"
        c['horario'] = busca_horario(c)
    else:
        c['horario'] = c.get('horario').lower()

    # Algunos curso no llevan bien el mes de inicio
    if not c.get('mes_inicio'):
        c['mes_inicio'] = mes_inicio(c)
    if not c.get('certif_prof'):
        c['certif_prof'] = 'No'


    # gestión entidades
    _id_entidad = id_entidad(c.get('entidad'))
    _entidad = _entidades.get(_id_entidad)
    if not _entidad:
        _entidad = {'id':_id_entidad, "type": "ent"}
        _entidad['label'] = c.get('entidad').replace(_id_entidad, '').strip()
    _entidad['cursos'] = _entidad.get('cursos', 0) + 1
    _entidad['horas'] = _entidad.get('horas', 0) + int(c.get('horas'))
    _entidades[_id_entidad] = _entidad


    _centro = lcentros[centro]
    _centro['localidad'] = c['localidad']
    _centro['direccion'] = c['direccion']
    _centro['id_entidad'] = _id_entidad
    _centro['id'] = _id_entidad + id_entidad(c.get('centro'))
    _centro['label'] = nombre_centro(centro)
    if _centro not in _centros:
        _centros.append(_centro)
    c['id_centro'] = _centro['id']

    del c['centro']
    del c['direccion']
    del c['localidad']
    del c['entidad']
    del c['nombre']
    del c['fecha_inicio']
    del c['fecha_fin']
    c['type'] = 'curso'
    if c.get('url_preinscr'):
            c['preinscr'] = 'Sí'
            del c['url_preinscr']
    #else:
    #    del c['num_preinscr']





    '''
    except e:
        print e
        c['localizacion'] = ''
        c['latlon'] = ""
    '''

lcur.extend(_centros)
cursos.update(metadatos)

lentidades = _entidades.values()
#lcur.extend(lentidades)
entidades = {'items':lentidades}
entidades.update(metadatos_ent)


json.dump(cursos, open('inaem.js', 'w'))
json.dump(entidades, open('entidades.js', 'w'))

json.dump(lcentros, open('inaem_centros.js', 'w'))


locale.setlocale(locale.LC_TIME, 'es_ES.utf8')

ahora = datetime.now()

def pon_hora(ahorat):
    texto_hora = '*****'
    ahora = ahorat.strftime("%d de %B de %y a las %H:00")
    open('index.html', 'w').write(open('inaem.html').read().replace(texto_hora, ahora))

pon_hora(ahora)
